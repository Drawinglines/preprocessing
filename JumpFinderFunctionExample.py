# -*- coding: utf-8 -*-
"""
JumpFinderFunction module example
This is a working example of the JumpFinder as located in the JumpFinderFunction module.

Created on Thu Mar 17 18:14:33 2016

@author: Saro
"""
from JumpFinderFunction import JumpFinder

def main():
    series = [0, 0, 1, 2, 3, 9, 7, 5, 4, 3, 2, 2.2, 1]
    jump_series = JumpFinder(series)
    print jump_series
    
main()
