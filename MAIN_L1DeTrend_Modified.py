# -*- coding: utf-8 -*-
"""
Created on Sat Dec 12 15:44:04 2015


Whats new?

Can write data to DB in format reqd by Taha or Kulbir by calling respective functions

- NowDate : Dates till which records should be fetched. Excludes the data in the NowDate.
- WindowSize : how many days to look back after CF
- RefThrshYears



@author: Reza_3
◙"""

#DB Module
import MySQLdb

# my modules
import reza_helpers as reza_hp
import DBWriters
import core_L1DeTrend as core_L1DeTrend

# python modules computation
import numpy as np
from collections import defaultdict
import cPickle as pk
from datetime import datetime
from JumpFinder import JumpFinder

# python modules visualization
import pylab
#import cProfile
#import gprof2dot
#from graphviz import Digraph
#from bprofile import BProfile
import time

#from numba import jit
#from numpy import arange


#------------------------------
# parameterz and initialization
#NowDate = datetime.datetime.now()
NowDate = "2015-04-30"

DB_ProdTable_name   = "big_prod_sum2" 
DB_MasterTable_name = "big_master"
ProdSchema          = "public_data"


# Writing Results to the following table and schema
Wr_Schema                  = 'saro'
DB_wrTable_TimeSeries      = "The100Wells"  # Create a table in the database first
DB_wrTable_SQL             = "The100Wells"
#Wr_ProdTablename   = 'CF_Taggs_ND'
#Wr_MasterTablename = 'CF_Taggs_ND_master'
    
    
#NowDate       = (str(now.year)+"-"+str(now.month)+"-"+str(now.day)) # "2015-04-30"
NowDate         = "2015-04-30"
TrainStartDate  = "1900-01-01"
TrainEndDate    = "2015-04-30"

RefThrshYears = 1*12 # number of month if well was down at the end, then its CF
Time_Trsh     = 2    # number of month the well should have been producing to be used for analysis
Diff_days     = 2    # tolerance on number of down days to be accepted as downevent
StateAPI      = 33   # for NorthDa♣kuta = 33
#------------------------------




#@jit
def main():  
    t0 = time.time()
    #----------------------
    # initialize
    pylab.close('all')
    #----------------------
    #-----------------------------------------
    # Establish the connections
#    conn,cursor     = reza_hp.establish_DBconn(ProdSchema)
#    connwr,wrcursor = reza_hp.establish_DBconn(Wr_Schema)
    config = {"host":"navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com",'user':"saro",'password':'saro@usc'}
    cnx = { 'host': config["host"],'user':config["user"],'password': config["password"],'db': ProdSchema}    
    conn = MySQLdb.connect(cnx['host'],cnx['user'],cnx['password'], cnx['db'])
    cursor = conn.cursor() 
    cnx2 = { 'host': config["host"],'user':config["user"],'password': config["password"],'db':Wr_Schema}    
    connwr = MySQLdb.connect(cnx2['host'],cnx2['user'],cnx2['password'], cnx2['db'])
    wrcursor = connwr.cursor() 
    #-----------------------------------------
    # Drop table if exists
    try:
        query = "DROP TABLE {};".format(Wr_Schema+'.'+DB_wrTable_TimeSeries)
        wrcursor.execute(query)
    except:
        print "Table Doesnt Exist"
    #query = "DROP TABLE IF EXISTS {};".format(Wr_Schema+'.'+Wr_MasterTablename)
    #wrcursor.execute(query)
    #-----------------------------------------        
            
    
    
    
    
    #-----------------------------------------
    #load Unique Pids
    #unique_pids = reza_hp.get_WellPids(cursor, DB_ProdTable_name, DB_MasterTable_name, ProdSchema, StateAPI)
    unique_pids = reza_hp.get_TestingWellPids('100WellAPIsForTrendFilteringFromKulbir.txt')
    
    
    
    NumofWells = len(unique_pids) #max number of wells to be analysed
    #---------------------------------------------
    for i in xrange(0,NumofWells): #NumofWells
        
        #-----------------------------------------
        #Wells to be analyzed
        print ("Reading Well Number: "+str(i)+" outof("+str(NumofWells-1)+")      With name: "+str(long(unique_pids[i])))
        
        
        
        
        try:
            # read data from DB and preprocess it
            dataorig    = reza_hp.get_WellData(cursor,DB_ProdTable_name,DB_MasterTable_name,ProdSchema,unique_pids[i],TrainStartDate,TrainEndDate) # well production data
            data , flag = reza_hp.Preprocessing_DataDict(dataorig , Time_Trsh , Diff_days)
            
            if flag==0: # well has not produced (not good for analysis)
                continue
            
            
            
            #-------------------------------------------------
            #-------------------------------------------------
            #Physics Based Fitting
            keys = ['oil','water','wor','cumoil','cumwater']
            Trends_Dict = core_L1DeTrend.L1DeTrend(data,keys)
            #Trends_Dict = core_L1DeTrend.L1DeTrend_Parallel(data,keys)
            #-------------------------------------------------
            
            
            
            
            
            
            
            
            
            
            #-------------------------------------------------
            # gather needed data to be written to DB
            # Prepare dictionary data for writing
            
            WrDict = defaultdict(list)
            
            
            WrDict.update(Trends_Dict)
            
            
            
            WrDict['API']        = data['API']
            WrDict['dates']      = data['dates']
            WrDict['proddays']   = data['proddays']
            
            
            WrDict['oil']        = data['oil']
            WrDict['water']      = data['water']
            WrDict['wor']        = data['wor']
            WrDict['cumoil']     = data['cumoil']
            WrDict['cumwater']   = data['cumwater']
            
    
            
            
            
            #WrDict = copy.copy(data)      
            
            #-------------------------------------------------
            # Show the results in a plot
            
            #-------------------------------------------------
            # Show the results in a plot
            
            '''   
            pylab.subplot(1,1,1)
            
            vec = data['oil']
            pylab.plot(range(len(vec)),vec, 'bo')
            
            vec = Trends_Dict['oil_trend']
            pylab.plot(range(len(vec)), vec, 'r-')
            
            #pylab.plot(range(len(dataFore['lower'])), dataFore['lower'],'--', color='c', label='P-10')
            #pylab.plot(range(len(dataFore['lower'])), dataFore['upper'],'--', color='c', label='P-90')
            
            pylab.show()
            '''       
            
            #-------------------------------------------------
            
        
            
                
                
                
            
            #-------------------------------------------------
            # Write Forecasts To DataBase
            # print "Writing results"
            DBWriters.Write_Dict_ToDB(connwr,wrcursor,WrDict,Wr_Schema,DB_wrTable_TimeSeries,"SQL")
            #-------------------------------------------------
            
            
            del data , dataorig , WrDict
        except:
            print "Well " + str(long(unique_pids[i])) + " had an error."
            continue
    #-------------------------------------------------
    # Close the DB connection
    cursor.close()
    conn.close()
    wrcursor.close()
    connwr.close()
    
    # time the code
    t1 = time.time()
    delt_t = t1-t0
    print ("Total Execution Time: ("+str(delt_t) + ") Seconds")

if __name__=='__main__':
#    profiler = BProfile('L1DTrend_Profiling.png')
#    with profiler:
#        main()
    main()