# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 2016

@author: Saro
"""
import numpy as np

def PeakFinder_helper(series, alpha=0.5, beta=0.5, gamma=0.05):
    """" 
    This is a parametrized peak-finding algorithm. The fact that it is parametrized allows it to be trained on a data set.
    The use of parameters an extension of the keyword logic originally discussed.
    At this point, a series that has been trend-filtered us L1 penalties is expected, but other series can also be used.
    Additional conditions and parameters can be easily added to the if statement below.
    
    INPUT:
        -series: the list of numbers in which to find a peak. 
        -alpha: the proportion of the maximum above which to search for a peak e.g. 
         if alpha = 0.5 and the global maximum is 7, a point must have a height of at least
         alpha*maximum = 0.5*7 = 3.5 to be considered a peak.
        -beta: the minimum relative slope change for a peak to be found e.g. if beta is 0.5, the backward difference
         slope at the current point is 10, the backward difference slope at the previous point was 0,
         and the stability term was 0.001, then (10-0)/(0+0.001) > 0.5 and the point is eligible to be considered
         a peak (must also satisfy the other conditions).
        -gamma: the minimum relative function change for a peak to be found e.g. if gamma is 0.05, the function value
         at the current point is 20, the function value at the previous point was 2,
         and the stability term was 0.001, then (20-2)/(2+0.001) > 0.05 and the point is eligible to be considered
         a peak (must also satisfy the other conditions).
    
    OUTPUT:
        -peak_series_dict: a dictionary containing a list consisting of the indices of series where a peak was found and
         a list (with 1's and 0's) of the same length as series, with a 1 signifying a peak at that index.
            'indices': has the indexes of peaks
            'binary': has the 1's and 0's
    """
    # If there is only one point in the series, return an empty dictionary.
    peak_series_dict = {'indices':[],'binary':[]}
    n = len(series)
    if n <= 1:
        return peak_series_dict
    
    # Declare variables.
    sC = 0.001 # Stability term
    peak_binary = [0]*n
    peak_indices = []
 
    # Calculate the slope. Use a backward difference. Set the first point's slope to 0.
    slope = [series[i]-series[i-1] for i in xrange(1,n)]
    slope.insert(0,0)
    
    # Calculate the maximum.
    maximum = np.max(np.asarray(series)).tolist()
    
    # If there is a decrease at the first point, it is automatically considered a peak.
    # This is done because the first point may actually be the highest point in production,
    # but might not pass the conditions below due to a lack of previous points.
    if series[0] > series[1]:
        peak_binary[0] = 1
        peak_indices.append(0)
    
    # Find the indices of the local maxima. This means that the slope increases and then decreases.
    # The exception is the first point. As written, the last point cannot qualify as a maximum.
    local_maxima = []
    for i in xrange(1,n-1):
        if slope[i] >0 and slope[i+1] < 0:
            local_maxima.append(i)
    
    # Apply the parameters. Store the results.
    local_max_len = len(local_maxima)
    for i in xrange(0,local_max_len):
        index = local_maxima[i]
        if (
            # ***************Add additional conditions here.************
            # Check the relative slope increase condition (beta).
            (slope[index]-slope[index-1])/(slope[index-1] + sC) > beta and
            # Check the minimum relative function increase condition (gamma).
            (series[index]-series[index-1])/(series[index-1] + sC) > gamma and
            # Check the proportion of maximum condition (alpha).
            (series[index] > alpha*maximum)
            ):
                peak_indices.append(index)
                peak_binary[index] = 1
                
    # Write the results to a dictionary and return.
    peak_series_dict['indices'] = peak_indices
    peak_series_dict['binary'] = peak_binary
           
    return peak_series_dict

# Test. series[3] should be identified as a peak.
# print PeakFinder_helper([0, 0 ,0, 35, 5, 4, 3, 2, 1])