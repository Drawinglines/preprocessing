# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:03:16 2015
Combinatorics helper 
Reading from the database
@author: Administrator
"""

import numpy as np
from datetime import datetime
import calendar


    


def diff_month(d1str,d2str):
    d1 = datetime.strptime(d1str , '%Y-%m-%d')
    d2 = datetime.strptime(d2str , '%Y-%m-%d')
    try:
        return (d2.year - d1.year)*12 + d2.month - d1.month
    except:
        import pdb; pdb.set_trace()
        print "Exception.  The argument was {}.".format(d2)
        return 0 #np.NaN
        
def add_months(sourcedatestr,months):
    sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d')    
    
    month = [ sourcedate.month - 1 + months[i] for i in xrange(len(months)) ]
    year  = [int(sourcedate.year + x / 12 ) for x in month]
    month = [ month[i] % 12 + 1 for i in xrange(len(month)) ]
    day   = [min(sourcedate.day , calendar.monthrange(year[i],month[i])[1]) for i in xrange(len(year)) ]
    
    datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in xrange(len(year))]   
    dates      = [datetime.strptime(datestring[i] , '%Y-%m-%d') for i in xrange(len(datestring))]    
    return datestring , dates
    
def imputeMissingData(dataIn):
    """finds missing months in the Welldates and inserts 0 for qoil, qwat at the corresponding indexes"""
    
    # determine the number of month between first and last dates given in data
    WellStartDate = dataIn['dates'][0] 
    monthnumsIndx = np.array([diff_month(WellStartDate,x) for x in dataIn['dates']])
    
    # generate the continuse month dates strings for the new data
    allindx       = range(max(monthnumsIndx)+1)
    datesstr,_    = add_months(WellStartDate,allindx)
    
    # imput nans for missing data
    dataOut = dataIn.copy()
    for (i , key) in enumerate(dataIn):
        # Update the measured values series by imputing NaNs when data is missing
        dataOut[key] = np.empty(shape=len(datesstr) , dtype=type(dataIn[key]))#np.array(dataIn[key],dtype=type(dataIn[key][0]),ndmin=len(datesstr))
        dataOut[key].fill(0.0) # can change into zero if want 0
        dataOut[key][monthnumsIndx] = dataIn[key]

    
    # get linear time index of months and the continuous dates strings
    dataOut['MonthIndx'] = np.asarray(range(np.nanmax(monthnumsIndx)+1))  #range(monthnumsIndx[-1])
    dataOut['dates']     = np.asarray(datesstr)
    
    return dataOut

def inverse_cumsum(cumulative):
    output = [0] * len(cumulative)
    
    for i in xrange(len(cumulative)-1):
        output[i+1] = cumulative[i+1] - cumulative[i]
    output[0]=cumulative[0]
    
    return output

def matches(a,b):
    """function to objectively test how similar the decoded values are of qoil and qwat"""
    c = [i for i,j in zip(a,b) if i==j]
    return round((float(len(c))/len(a))*100,2)