# -*- coding: utf-8 -*-
"""
Created on Fri Feb 19 11:37:12 2016

@author: Administrator
"""
#import warnings
#warnings.filterwarnings('error')

#== data storage and manipulation
from collections import defaultdict

#== computation
from numpy.linalg import norm, solve
import cvxpy as cvx
#import pyOpt
import scipy as sp
from scipy.optimize import minimize
from scipy.sparse import csr_matrix
import numpy as np
import cvxopt

#== parallel processing
import joblib

# my modules
import reza_helpers as reza_hp

##############################################################################################################################################################
 
 
def lambdaMax(y):
    """ This function find the smallest lambda that makes the piecewise approximation
    a single line.
    """
    T = y.shape[0]
    I = np.eye(T-2)
    O = np.zeros((T-2,1))
    D = np.concatenate([I,O,O], axis=1)\
        + np.concatenate([O,-2*I,O], axis=1)\
        + np.concatenate([O,O,I], axis=1)
    return norm(solve(np.dot(D,D.transpose()), D*y), np.inf)

def tsSegment(x, D):
    """ This function computes the indexes of x in which a change point has happened.
    """
    z = np.abs(np.dot(D, x))        # Taking the second order difference of x
    th = 0.1
    z[ z < th*np.mean(z) ] = 0      # Removing the very small changes
    knots = np.nonzero(z)[0] + 1 # KRZ
    
    return knots

def step_representation(x, y):
    x = x.flatten()
    y = y.flatten()
    criticals = np.concatenate([[0], np.where(np.diff(y, n=2)>1e-6)[0], [y.size-1]])
    lengths = np.diff(x[criticals])
    slopes = np.diff(y[criticals])/lengths
    return (slopes, lengths)
    
    
    
##############################################################################################################################################################



def creat_DMatrix(size , order=1):
    #== method 1    
    #D = np.concatenate( [np.diag(np.ones((size-2,))), np.zeros((size-2,2))], axis=1) \
    #     + np.concatenate( [np.zeros((size-2,1)), np.diag(-2*np.ones((size-2,))), np.zeros((size-2,1))], axis=1) \
    #     + np.concatenate( [np.zeros((size-2,2)), np.diag(np.ones((size-2,)))], axis=1)
    
    
    #== method 2
    D=tridiag(size,-1,2,-1)
    D = D**order
    np.delete(D,0,0)
    np.delete(D,-1,0)  
    
    
    #== method 3
    # Form second difference matrix.
    #e = np.mat(np.ones((1, size)))
    #D = sp.sparse.spdiags(np.vstack((e, -2*e, e)), range(3), size-2, size)
    # Convert D to cvxopt sparse format, due to bug in scipy which prevents
    # overloading neccessary for CVXPY. Use COOrdinate format as intermediate.
    #D_coo = D.tocoo()
    #D = cvxopt.spmatrix(D_coo.data, D_coo.row.tolist(), D_coo.col.tolist())
    
    
    Dmat = csr_matrix(np.asarray(D,float)) 

    return Dmat




def creat_ZMatrix(y , FixKnt=[]):    
        
    f1_knots = [i for i in xrange(len(y)) if y[i]<=0.1] # knots for zero places in y vector
    FixKnt.extend(f1_knots) # add zero value knots to given knots
    
    knots_flag = np.zeros([len(y),1])
    for i in FixKnt:
        knots_flag[i]=1.0
        
    Z=np.diag([1]*len(y))  
    for i,val in enumerate(knots_flag):
        if val==1.0:
            Z[i][i]=0 # if there is a knot then dont exclude the corresponding y value from trend
    Z = csr_matrix(np.asarray(Z,float))  
    return Z.todense()
     
def tridiag(T,x,y,z,k1=-1, k2=0, k3=1):
    a = [x]*(T-abs(k1)); b = [y]*(T-abs(k2)); c = [z]*(T-abs(k3))
    TriDmat = csr_matrix (np.diag(a, k1) + np.diag(b, k2) + np.diag(c, k3))
    
    return TriDmat.todense()

    
##############################################################################################################################################################

def L1DeTrend (data,keys, lambda_frac):
    Trends_Dict = defaultdict(list)
    #======= Gather y
    for i,item in enumerate(keys):
        #print item
        
        y = data[item]
        y = np.asarray(y,float).flatten()
        
        #======= calculate fixedknots
        #======= get proddays
        DaysDown,_= reza_hp.DownTime(data['dates'],data['proddays'])
        fixed_knots = []
        fixed_knots = [i for i in xrange(len(DaysDown)) if DaysDown[i]<2]
    
        #======= Preprocess
        #try:
        y = np.log(np.abs(y)+1.0)
        flag = 1
        #except:
        #    flag = 0
        
        #======= calc D and Z
        T = np.shape(y)[0]
        D = creat_DMatrix(T,order=1)
        Z = creat_ZMatrix(y , FixKnt = fixed_knots)
        
        #======= Set parameters
        lam_max = lambdaMax(np.transpose(np.dot(Z,y)))
        lambda_val = lambda_frac*lam_max
        sC         = 0.000 # for stability
        
        
        #======= CalcTrend optim
        trend = TrendOptim_CVX(y , Z , D , lambda_val , sC)
        #trend = TrendOptim_SciPy(y , Z , D , lambda_val , sC)
        #trend = TrendOptim_PICOS(y , Z , D , lambda_val , sC) 
        
        # get the slopes
        #slopes, lengths = step_representation(times, trend)
        temp  = [t - s for s, t in zip(trend, trend[1:])]
        temp.append(0)
        slope = np.array(temp,float)
        del temp
        
        point_slope = [t - s for s, t in zip(y, y[1:])]
        point_slope.append(0) #to maintain length of list
        point_slope = np.array(point_slope,float)
        
        
    
        #======= Postprocess
        if True: #flag==1 :
            trend = np.exp(trend)-1.0
            slope = np.multiply(trend+1.0 , slope)
            point_slope = np.multiply(y+1.0 , point_slope)
        
        
        #======= gather trends
        Trends_Dict[item+"_trend"] = np.asarray(trend,float).flatten()
        Trends_Dict[item+"_slope"] = np.asarray(slope,float).flatten()
        Trends_Dict[item+"_pointSlope"] = np.asarray(point_slope,float).flatten()
        
        del trend ,slope ,point_slope
        
    return Trends_Dict
    
    
    
    
def TrendOptim_CVX(b , A , D , lambda_val=0.0 , sC=0.0):
    #===========
    T         = len(b)  
    #=========== Using CVX Package
    x         = cvx.Variable(T)
    x.value   = b
    
    #========== Form the objective function and constraints
    #objective = cvx.Minimize(cvx.sum_squares(A*(x-b)) + lambda_val*cvx.norm(D*x,1) + sC*cvx.norm(x,2)**2)
    #objective = cvx.Minimize( cvx.norm(A*(x-b),1) + lambda_val*cvx.norm(D*x,1) + sC*cvx.norm(x,2)**2 ) # for robust trending and removing outliers
    objective = cvx.Minimize(cvx.sum_entries(cvx.huber(A*(x-b))) + lambda_val*cvx.norm(D*x,1))
    
    constraints = []
    B           = np.eye(T)-A
    constraints = constraints + [np.nanmin(b) <= x, x <= np.nanmax(b)]   
    constraints = constraints + [B*x==np.transpose(np.dot(B,b))]
    prob        = cvx.Problem(objective,constraints)
    #prob      = cvx.Problem(objective)
    #prob      = cvx.Problem(cvx.Minimize(cvx.norm(D*x,1)),[A*x==b])
    
    
    #========= solve the problem
    #prob.solve(solver = cvx.CVXOPT) # very bad solver
    
    #print prob.is_dcp()
    try:
        
        # ECOS and SCS solvers fail to converge before
        # the iteration limit. Use CVXOPT instead.
        #print prob.is_dcp()
    #prob.solve(solver=cvx.SCS , max_iters=200 , warm_start=True)
        prob.solve(solver=cvx.ECOS , max_iters=200 , warm_start=True)
    
    except:
        prob.solve(solver=cvx.SCS , max_iters=200 , warm_start=True)
        #prob.solve(solver=cvx.SCS)
        #if prob.status!=cvx.OPTIMAL:
        #    prob.solve(solver=cvx.ECOS)
        #raise Exception("Solver did not converge!")
    
    #======== get the results out
    trend = np.squeeze(np.asarray(x.value,float))
    return trend


    
def TrendOptim_SciPy(y , Z , D , lambda_val=0.0 , sC=0.0):
    #===========
    T         = len(y)  
    def objfunc(x , *args ):
        try:
            objval = np.linalg.norm( np.dot(Z,x)-y )**2 + lambda_val*np.linalg.norm(np.dot(D,x),1) + sC*np.linalg.norm(x)**2
            fail = 0
        except:
            fail = 1       
        g      = []
        return objval
    #=========== use Scipy optimization package
    res = minimize(objfunc , y , method='BFGS')
    #=========== solution
    trend = np.array(res.x,float)
    return trend
    
##############################################################################################################################################################

def DeTrendWrapper(y,argDict):
    #======= resize y
    y = np.asarray(y,float).flatten()
    #======= Preprocess
    y = np.log(np.abs(y)+1.0)
    
    #======= calculate fixedknots
    fixed_knots = []
    DaysDown,_    = argDict['DaysDown']
    fixed_knots = [i for i in xrange(len(DaysDown)) if DaysDown[i]>2]    
    #======= calc D and Z
    T = np.shape(y)[0]
    D = creat_DMatrix(T,order=1)
    Z = creat_ZMatrix(y , FixKnt = fixed_knots)
    
    #======= Set parameters
    lam_max = lambdaMax(np.transpose(np.dot(Z,y)))
    lambda_val = 0.002*lam_max
    sC         = 0.00002 # for stability
    #======= CalcTrend optim
    trend = TrendOptim_CVX (y , Z , D , lambda_val , sC)
    
    
    # get the trend and slopes
    temp  = [t - s for s, t in zip(trend, trend[1:])]
    temp.append(0)
    slope = np.array(temp,float)
    del temp
    
    point_slope = [t - s for s, t in zip(y, y[1:])]
    point_slope.append(0) #to maintain length of list
    point_slope = np.array(point_slope,float)
    
    #======= Postprocess
    trend = np.exp(trend)-1.0
    slope = (trend+1.0)*slope
    point_slope = (y+1.0)*point_slope
    
    
    
    return trend , slope , point_slope 
        
        
        
def L1DeTrend_Parallel (data,keys):
    Trends_Dict = defaultdict(list)
    
    #======= get proddays
    DaysDown = reza_hp.DownTime(data['dates'],data['proddays'])
    argDict = {'DaysDown':DaysDown , 'proddays':data['proddays'] , 'dates':data['dates']}
    #======= Gather y
    trajects = [data[item] for item in keys]
    #======= find trends in parallel
    parallel_tuple = joblib.Parallel(n_jobs=10, backend="threading")(joblib.delayed(DeTrendWrapper)(item,argDict) for item in trajects)
    #======= gather trends
    for i,item in enumerate(keys):
        Trends_Dict[item+"_trend"] = np.asarray(parallel_tuple[i][0]).flatten()
        Trends_Dict[item+"_slope"] = np.asarray(parallel_tuple[i][1]).flatten()
        Trends_Dict[item+"_pointSlope"] = np.asarray(parallel_tuple[i][2]).flatten()
    del parallel_tuple
    return Trends_Dict
