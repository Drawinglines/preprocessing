# -*- coding: utf-8 -*-
"""
Created on Mon Feb 29 10:11:19 2016

@author: Administrator
"""

CommonDir = ''
import sys
sys.path.append(CommonDir+'Helpers') 


import DBWrite_helpers as DB_hp
import reza_helpers as reza_hp
import cPickle as pk
from collections import defaultdict

# Writing Results to the following table and schema
Wr_Schema        = 'results'
DB_TBWrite       = 'CFTags_NewMexico_SQL' #"CFTags_ND_NoSQLBased"  
    

#======= read data file
with open('Results/NewMexico_AnalizedCFTags_tuple.p', 'rb') as fp:
    data = pk.load(fp)
 



#======== write to DB
#-----------------------------------------
# Establish the connections
connwr,wrcursor = reza_hp.establish_DBconn(Wr_Schema)
#-----------------------------------------
# Drop table if exists
try:
    query = "DROP TABLE {};".format(Wr_Schema+'.'+DB_TBWrite)
    wrcursor.execute(query)
except:
    print "Table Doesnt Exist"
    
        
for i,TempDict in enumerate(data):  
    print ("writing Well Number: "+str(i)+" outof("+str(len(data)-1)+")" )
    #============ Extract the attrobutes needed for specific purposes
    WrDict = defaultdict(list)
    
    WrDict['API']        = data[i]['API'][0]              
    WrDict['CF_flag']    = data[i]['CF_flag']
    WrDict['RunLife']    = data[i]['RunLife']
    WrDict['EndDate']    = data[i]['EndDate']
    WrDict['StartDate']  = data[i]['StartDate']
    
    
    #WrDict['oil_pd']     = data[i]['oil']
    #WrDict['water_pd']   = data[i]['water']
    #WrDict['wor_pd']     = data[i]['wor']
    #WrDict['cumoil']     = data[i]['cumoil']
    #WrDict['cumwater']   = data[i]['cumwater']
    
    #WrDict['oilprice']   = data[i]['oilprice']
    #WrDict['lat']        = data[i]['latitude']
    #WrDict['longi']      = data[i]['longitude']
    #WrDict['proddays']   = data[i]['proddays']
    #WrDict['dates']      = data[i]['dates']
    #WrDict['pool_name']  = data[i]['pool_name']
        
    
    #=========== write to DB
    _,_=DB_hp.Write_Dict_ToDB(WrDict,Wr_Schema,DB_TBWrite,connwr,wrcursor,Format="SQL")
    
    del WrDict
    
#======= Close the DB connection
wrcursor.close()
connwr.close()    
    
    
