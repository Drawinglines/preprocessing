# -*- coding: utf-8 -*-
"""
Created on Thu Mar 17 15:43:30 2016

@author: Saro
"""
import numpy as np

def JumpFinder(series):
    """" 
    Finds large relative changes in the value of a list of numbers.
    Intended to find the location of large positive changes in slope in an L1-trend filtered time series.
    series = function value (e.g. oil/water rate, cumulative oil/water production)
    
    This function looks for peaks that are large compared to the global time series.
    
    INPUT: A list of numbers.
    OUTPUT: A list of 0's and 1's (numbers) where 1's indicate the presence of a jump
            and 0's indicate a lack of a jump.
    """
    threshold = 0.5 # Relative change limit for the slope
    threshold2 = 0.05 # Relative change limit for the function value.
    sC = 0.001 # Stability term
    n = len(series)
    jump_series = [0]*n
    # Calculate the slope. Use a forward difference. Set the last point's slope to 0.    
    slope = [series[i+1]-series[i] for i in xrange(0,n-1)]
    slope.append(0)
    
    # Calculate the median.
    med = np.median(np.asarray(series)).tolist()
    
    # Set the maximum value to be a jump.
    max_indx = series.index(max(series))
    if series[max_indx] >0:
        jump_series[max_indx] = 1
     
    # Look for relatively large positive changes in slope to indicate a jump.
    for i in xrange(1,n-1):
        try:
            if abs((slope[i]-slope[i-1]) /(slope[i-1]+sC)) > threshold and slope[i] > 0 and (series[i]-series[i-1])/(series[i-1]+sC) > threshold2 and series[i] != series[i-1] and series[i]>med:
                while series[i+1]>series[i]:
                    i += 1
                jump_series[i] = 1
        except:
            jump_series[i] = 0
           
    return jump_series