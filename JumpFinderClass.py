# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 19:34:07 2016

JumpFinder Class
This class is used to find the jumps in a given time series. The time series is
assumed to have equal time steps throughout the data.

Logic customization and built-in properties are also available.

INPUT: 
- series: A list of numbers
- statement: A logical query for the JumpFinder to compute.

@author: Saro
"""
import numpy as np

class JumpFinder:
    
    # The valid logical keywords.
    _keywords = ["FWD", "BWD","CENT","PEAKS","BEGINNING_OF_JUMPS","ABOVE_GLOBE_MED",
                "ABOVE_GLOBE_MEAN","LARGE_SLOPE_CHANGE", "LARGE_REL_SLOPE_CHANGE", "FUNCTION_JUMP",
                "BELOW_GLOBE_MED", "BELOW_GLOBE_MEAN", "MAX"]
    
    # The initializer.
    def __init__(self,series,statement):
        self._length = len(series)
        self._series = series
        self._logic = statement
        try:
            self._calculate_jumps()
        except:
            print "Error in initializing JumpFinder. Please correct the error and reinitialize."
    
    # Some methods to interact with the JumpFinder.
    # Get the slope via a forward finite difference.
    def get_slope_fwd(self):
        _temp = [float(self._series[i+1]-self._series[i]) for i in xrange(0,self._length-1)]
        _temp.append(0)
        return _temp
    
    # Get the slope via a backward finite difference.
    def get_slope_bwd(self):
        _temp = [0]
        _temp2 = [float(self._series[i]-self._series[i-1]) for i in xrange(1,self._length)]
        _temp.extend(_temp2)
        return _temp
        
    # Get the slope via a central finite difference.
    def get_slope_cent(self):
        _temp = [0]
        _temp2 = [float(self._series[i+1]-self._series[i-1])/2 for i in xrange(1,self._length-1)]
        _temp.extend(_temp2).append(0)
        return _temp
    
    # Get the series itself.
    def get_series(self):
        return self._series
        
    # Get the logical command used.
    def get_logic(self):
        return self._logic
    
    # Change the series (but not the logic) and recalculate the jumps.
    def change_series(self,series):
        temp = self._length
        temp2 = self._series
        self._length = len(series)
        self._series = series
        try:
            self._calculate_jumps()
        except:
            print "Error in applying new series. Please correct and retry."
            self._length = temp
            self._series = temp2
    
    # Change the logic (but not the series) and recalculate the jumps.
    def change_logic(self, statement):
        temp = self._logic
        self._logic = statement
        try:
            self._calculate_jumps()
        except:
            print "Error in applying new logic. Please correct and retry."
            self._logic = temp
    
    # Get the slope that was used in calculating the jumps.
    def get_used_slopes(self):
        return self._slope
    
    # Get a list of where the jumps occured.
    # A "1" indicates a jump.
    # A "0" indicates a lack of a jump.
    def get_jumps(self):
        return self._jumpsList
        
    # Calculate the jumps in the series based on the logic provided.
    # A "1" indicates a jump.
    # A "0" indicates a lack of a jump.
    # THIS IS A NON-PUBLIC FUNCTION.
    def _calculate_jumps(self):
        # Initialize the jump series.
        jump_series = [1]*self._length
        # Get the keywords from the logic statement input.
        input_keywords = self._logic.split(" ")
        
        # Check to make sure all the input keywords are part of the permitted
        # keywords of the class.
        for keyword in input_keywords:
            if keyword not in self._keywords:
                raise NameError("Unrecognized keyword found in the logical statement.")
        
        # Get the requested slope. Note that in case multiple slope specifications
        # are present, override occurs as follows.
        if "FWD" in input_keywords:
            self._slope = self.get_slope_fwd()
        elif "BWD" in input_keywords:
            self._slope = self.get_slope_bwd()
        elif "CENT" in input_keywords:
            self._slope = self.get_slope_cent()
        else:
            raise NameError("No slope specification found in the logical statement.")
        
        # Check for median condition. Note the override order.
        series_np = np.asarray(self._series)
        series_np_median = np.median(series_np)
        series_np_mean = np.mean(series_np)
        jump_series_np = np.asarray(jump_series)
        
        if "ABOVE_GLOBE_MED" in input_keywords:
            jump_series = np.logical_and(np.greater(series_np,series_np_median),jump_series_np.astype(bool)).astype(int).tolist()
        elif "BELOW_GLOBE_MED" in input_keywords:
            jump_series = np.logical_and(np.less(series_np,series_np_median),jump_series_np.astype(bool)).astype(int).tolist()
               
        # Check for the mean condition. Note the override order.
        if "ABOVE_GLOBE_MEAN" in input_keywords:
            jump_series = np.logical_and(np.greater(series_np,series_np_mean),jump_series_np.astype(bool)).astype(int).tolist()
        elif "BELOW_GLOBE_MEAN" in input_keywords:
            jump_series = np.logical_and(np.less(series_np,series_np_mean),jump_series_np.astype(bool)).astype(int).tolist()
        
        # Check for a jump in the function value.
        threshold1 = 0.05 # Relative change limit for the function value.
        sC = 0.001 # Stability term                
        if "FUNCTION_JUMP" in input_keywords:
            for i in xrange(1,self._length):
                if not( (self._series[i]-self._series[i-1])/(self._series[i-1]+sC) > threshold1 and self._series[i] != self._series[i-1]):
                    jump_series[i] = 0
                    
        # Check for a large relative slope change.
        threshold2 = 0.5 # Relative change limit for the slope
        if "LARGE_REL_SLOPE_CHANGE" in input_keywords:
            for i in xrange(1,self._length-1):
                try:
                    if not(abs((self._slope[i]-self._slope[i-1]) /(self._slope[i-1]+sC)) > threshold2 and self._slope[i] > 0):
                        jump_series[i] = 0
                except:
                    jump_series[i] = 0
                    
        # Check for a large absolute slope change.
        threshold3 = 25 # Absolute change limit for the slope
        if "LARGE_SLOPE_CHANGE" in input_keywords:
            for i in xrange(1,self._length-1):
                if not(self._slope[i]-self._slope[i-1]> threshold3):
                    jump_series[i] = 0
        
        # Set the maximum point automatically to a jump if requested.
        if "MAX" in input_keywords:
            max_indx = self._series.index(max(self._series))
            if self._series[max_indx] > 0:
                jump_series[max_indx] = 1
        
        # Filter for peaks. Do this after the other filters are done. Note the override.
        if "PEAKS" in input_keywords:
            for i in xrange(1,self._length-1):
                if jump_series[i] == 1:
                    while self._series[i+1] > self._series[i]:
                        jump_series[i] = 0
                        i +=1
                    jump_series[i] = 1
                    
        elif "BEGINNING_OF_JUMPS" in input_keywords:
            # This option will filter out peaks in favor of the beginning of jumps.
            for i in xrange(1,self._length-1):
                if jump_series[i] == 1:
                    # Skip the first point.
                    i += 1
                    while self._series[i+1] > self._series[i]:
                        jump_series[i] = 0
                        i +=1
                    jump_series[i] = 0
                    
        # Assign the final result to the instance's jumpList.
        self._jumpsList = np.greater(np.asarray(jump_series),np.asarray([0])).astype(int).tolist()
 
# End of class JumpFinder       
def JumpFinder3(series):
    """"    
    Finds large relative changes in the value of a list of numbers.
    Intended to find the location of large positive changes in slope in an L1-trend filtered time series.
    series = function value (e.g. oil/water rate, cumulative oil/water production)
    
    This function looks for peaks that are large compared to the global time series.
    """
    threshold = 0.5 # Relative change limit for the slope
    threshold2 = 0.05 # Relative change limit for the function value.
    sC = 0.001 # Stability term
    n = len(series)
    jump_series = [0]*n
    # Calculate the slope. Use a forward difference. Set the last point's slope to 0.    
    slope = [series[i+1]-series[i] for i in xrange(0,n-1)]
    slope.append(0)
    
    # Calculate the median.
    med = np.median(np.asarray(series)).tolist()
    
    # Set the maximum value to be a jump.
    max_indx = series.index(max(series))
    if series[max_indx] >0:
        jump_series[max_indx] = 1
     
    # Look for relatively large positive changes in slope to indicate a jump.
    for i in xrange(1,n-1):
        try:
            if abs((slope[i]-slope[i-1]) /(slope[i-1]+sC)) > threshold and slope[i] > 0 and (series[i]-series[i-1])/(series[i-1]+sC) > threshold2 and series[i] != series[i-1] and series[i]>med:
                while series[i+1]>series[i]:
                    i += 1
                jump_series[i] = 1
        except:
            jump_series[i] = 0
           
    return jump_series
    
def write_Results2(cursor_write,conn_write,DB_Table_name,wellID,AnalyzedStructure,raw_data,xDim, yDim, JumpIndicator, proddays,Welldates,schema):
    """
    Writes the results back to the database. Also checks to see if all the results have the same length
    else pads it with null.
    
    Note that this function has been modified to work with one dimension at a time. The names are not hardcoded.
    """
    
    query_write=("INSERT INTO "+schema+"."+DB_Table_name+" (`API`,`Dates`,`raw_data`,`xDim`,\
    `yDim`,`data_PtWisSlope`,`yTrend`,`trend_PtWisSlope`,\
    `proddays_L1Decomp1`,`DaysDown_L1Decomp1`,`JumpIndicator`)\
    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);")

    trend_L1,slope_L1,PtWisSlope_L1 = AnalyzedStructure['yDim']
    DaysDown=AnalyzedStructure["DaysDown"]
    
    #Handle condition if len!=len(Welldates) by adding NULL
    for l in [proddays,DaysDown,raw_data,xDim, yDim, trend_L1,slope_L1,PtWisSlope_L1]:
        if len(l)!=len(Welldates):
            for j in xrange(len(Welldates)-len(l)):
                l.append("NULL")
                
    for k in xrange(len(Welldates)):
        cursor_write.execute(query_write,(wellID,Welldates[k],(raw_data[k]),(xDim[k]),(yDim[k]),(PtWisSlope_L1[k]),(trend_L1[k]),(slope_L1[k]),(proddays[k]),(DaysDown[k]),(JumpIndicator[k])))
        conn_write.commit()
    print str(wellID)+ " written to database"
    #print("Time Taken : "+str(round(((time.time()-start)),2))+" secs")
    print

# without profiler
#main()

# with profiler
#cProfile.runctx('main()',globals(),locals(),'L1DTrend_Profiling.pstats')